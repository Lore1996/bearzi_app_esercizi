// SCRIVI UNA FUNZIONE CHE INVERTE UN NUMERO (ES. 32243 -> 34223)
function inverti(numero){
    var arrNumero = numero.toString().split("");
    var invertito = "";
    for (i = arrNumero.length - 1; i >= 0; i--) {
        invertito += arrNumero[i];
    };
    invertito = parseInt(invertito);
    console.log("Il numero invertito è "+invertito+" ed è di tipo "+typeof invertito+".");
};

// SCRIVI UNA FUNZIONE CHE CONVERTE LA PRIMA LETTERA DI OGNI PAROLA IN UNA FRASE IN MAIUSCOLO
function primaLetteraMaiuscola(frase){
    var frase2 = "";
    var arrFrase = frase.split(" ");
    var arrLetters = [];

    for (i = 0; i < arrFrase.length; i++) {
        arrLetters[i] = arrFrase[i].split("");
        arrLetters[i][0] = arrLetters[i][0].toUpperCase();
        for (j = 0; j < arrLetters[i].length; j++) {
            frase2 += arrLetters[i][j];
        };
        frase2 += " ";
    };
    console.log("Frase con le iniziali maiuscole: "+frase2);
};

// SCRIVI UNA FUNZIONE CHE RESTITUISCE IL NUMERO DI VOCALI IN UNA FRASE
function vocali(frase){
    var fraseVocali = frase.split("");
    var vocaliCounter = 0;
    for (i = 0; i < fraseVocali.length; i++) {
        switch (fraseVocali[i]) {
            case "a":
            case "e":
            case "i":
            case "o":
            case "u":
                vocaliCounter += 1;
        };
    };
    console.log(">>>La frase ha " + vocaliCounter + " vocali.");
};

// SCRIVI UNA FUNZIONE CHE RESTITUISCE IL NUMERO DI VOLTE CHE COMPARE OGNI LETTERA IN UNA FRASE
function lettere(frase){
    // trasformo la frase in minuscolo
    frase=frase.toLowerCase();
    // trasformo la frase in un'array di caratteri singoli
    var arr_caratteri = frase.split("");
    // creu un hash dove salvare il contatore di ogni lettera trovata
    var risultato = {}
    // definisco l'array dei caratteri ammessi
    var ammessi = ("abcdefghijklmnopqrstuvwxyzàèéòùì").split("");
    // faccio un loop su tutte le lettere
    for(var i=0; i<arr_caratteri.length; i++){
        // salvo in una variabile temporanea la lettera corrente
        var lett = arr_caratteri[i];
        if(ammessi.indexOf(lett)>=0){
            // aumento il contatore della lettera trovata
            if(risultato[lett]){
                risultato[lett] += 1;
            } else {
                risultato[lett] = 1;
            }
        }
        
    }
    // restituisco il risultato
    return risultato;
}

function stampaInConsole(oggetto){
    for (var i in oggetto) {
        var counter = oggetto[i];
        console.log("La lettera "+i+" compare "+counter+" volte nella frase");
    }
}

function stampaHTML(oggetto){
    var temp_arr = [];
    for (var i in oggetto) {
        var counter = oggetto[i];
        temp_arr.push("La lettera <b>"+i+"</b> compare <b>"+counter+"</b> volte nella frase.");
    }
    document.write("<p>"+temp_arr.join("</p><p>")+"</p>");
}

//definisco la frase da controllare
frase = "Phoenix of black quartz, judge my vow";
//richiamo la funzione
var risultato = lettere(frase);
stampaInConsole(risultato);
stampaHTML(risultato);
