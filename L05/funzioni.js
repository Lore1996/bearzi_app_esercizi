function sum(a,b,callback){
    var risultato = a + b;
    if(typeof callback == "function") callback(risultato);
    return risultato;
}

function quad(valore) {
    console.log("il quadrato di "+valore+" è "+(valore*valore));
}

function double(valore) {
    console.log("il doppio di "+valore+" è "+(valore+valore));
}


console.log(sum(5,6, quad));
console.log(sum(5,6, double));