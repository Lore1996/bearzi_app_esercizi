var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];

console.log("ARRAY INIZIALE: "+arr);
console.log("");

var removedItem = arr.pop(); //toglie l'ultimo elemento dall'array e lo inserisce nella variabile removedItem

console.log("Array.pop()");
console.log(arr);
console.log(removedItem+" è stato tolto.");
console.log("");

console.log("Array.reverse()");
var arrReversed = arr.reverse(); //restituisce l'array con gli elementi invertiti
console.log(arrReversed);
console.log("");

console.log("Array.shift()");
var removedItem = arr.shift(); //toglie il primo elemento dell'array
console.log(arr);
console.log(removedItem+" è stato tolto.");
console.log("");

console.log("Array.splice");
//
console.log("");

console.log("Array.unshift()");
arr.unshift(11, 12);
console.log(arr);
console.log("");

console.log("Array.join()");
var joined = arr.join(); //senza parametri usa la virgola come separatore
console.log(joined);
joined = arr.join("-");
console.log(joined);