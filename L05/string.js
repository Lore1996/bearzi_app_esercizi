var frase = "The quick brown fox jumps over the lazy dog."
console.log("LA FRASE è: " + frase);
console.log("");

console.log("String.charAt()"); //dato l'indice dice all'interno della stringa che carattere è
console.log(frase.charAt(0));
console.log("");

console.log("String.indexOf()");
console.log(frase.indexOf("j")); //cerco all'interno della stringa una lettera
console.log(frase.indexOf("jumps")); //posso cercare anche parole complete (è case-sensitive)
console.log("");

console.log("String.replace()"); //rimpiazza una stringa all'interno di un'altra stringa
console.log(frase.replace("jumps", "flies")); //.replace(searchValue, replaceValue) restituisce una stringa 
console.log("");