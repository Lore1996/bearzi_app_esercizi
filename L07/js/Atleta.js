class Atleta extends Persona {

    constructor(nome,eta,sesso,distanza,callbackArrivato) {
        /**
         * richiemo il costruttore del padre
         * passando i parametri ricevuti
        */
        super(nome,eta,sesso);
        // assegno le proprietà di istanza
        this.velocita = this._calcolaVelocita();
        this.callbackArrivato = callbackArrivato;
        this.distanzaPercorsa = 0;
        this.distanzaTotale = distanza;
        this.posizione = null;
        this.id = ++Atleta.tot_atleti;
        this.arrivato = false;
    }

    _calcolaVelocita() {
        var velocita = 6;
        var svantaggioEta = (this.eta-15)/(45)*2;
        var svantaggioSesso = (this.sesso=="Femmina") ? 0.3 : 0;
        var svantaggioCasuale = Math.random()/2;
        velocita -= svantaggioEta;
        velocita -= svantaggioSesso;
        velocita -= svantaggioCasuale;
        console.log(this.nome+" - "+this.eta+" - velocita: "+velocita);
        return velocita;
    }

    corri(secondi) {
        if(this.arrivato) return;
        this.distanzaPercorsa = secondi * this.velocita;
        if (this.distanzaPercorsa>=this.distanzaTotale){
            this.arrivato=true;
            this.callbackArrivato();
        }
    }
}

Atleta.tot_atleti = 0;