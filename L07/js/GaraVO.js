class GaraVO {
    constructor() {
        /**
         * nel caso arrivi dall'esterno
         * non ci sarebbe un costruttore
         * le dichiariamo per avere autocompletamento
         */
        this.atleti = null;
        this.distanza = null;
        this.vincitore = null;
    }
}