class Being {
    constructor (){ // il metodo constructor viene eseguito automaticamente ogni volta che un'istanza della classe viene creata
		console.log("new Being");
    }
    sayHi(){
        console.log("Hi, I am a being");
	}
}

class Animal extends Being { // extends rende la classe Animal figlia di Being
    constructor(age) {
        super();
		this.age = age;
		this.favourite_food = "";
		this.noise = "";
		/*
		var _this = this;
		setTimeout(function(){
			_this.makeNoise();
		},1000);
		console.log("new Animal");
		*/
		setTimeout(()=>this.makeNoise(), 1000);
		console.log("I am an Animal and I am "+this.age+" years old.")
    }
	makeNoise(){}
    run(){
        console.log("I am running");
    }
    eat(){
        console.log("I eat "+this.favourite_food);
    }
}

class Dog extends Animal {
	constructor(age){
		super(age)
		this.favourite_food = "Meat";
		this.noise = "Bau!";
		this._noiseInterval = setInterval(()=>this.makeNoise(), 2000);
		console.log("I am a Dog and I am "+this.age+" years old.");
	}
	shhht() {
		clearInterval(this._noiseInterval);
	}
	makeNoise() {
        console.log(this.noise);
    }
	eat() {
        super.eat();
        console.log(this.favourite_food+" and I like it very much");
    }
}

class Cat extends Animal {
	constructor(age){
		super(age)
		this.favourite_food = "Fish";
		this.noise = "Miao!";
		console.log("I am a Dog and I am "+this.age+" years old.");
	}
	sayHi() {
        console.log("Hi, I am a Cat and I am "+this.age);
	}
	makeNoise() {
        console.log(this.noise);
    }
	eat() {
        super.eat();
        console.log(this.favourite_food);
    }
}

var c = new Cat(3);
c.sayHi();
c.run();
c.eat();

console.log("---------");

var d = new Dog(2);
d.sayHi();
d.run();
d.eat();
d.makeNoise();