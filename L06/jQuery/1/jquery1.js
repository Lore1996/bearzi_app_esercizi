function cambiaColorePrimoLi(colore) {
    if (typeof colore === "object") {
        colore = colore.data;
    }
    $("li.primo").css("color", colore);
    return false;
}

/* function addClassLC() {
    $("div#contenitore").addClass("lc");
    return false;
} */

function toggleClassLC() {
    $("div#contenitore").toggleClass("lc");
    return false;
}

function spostaElemento() {
    var elemento = $("#ul1").find(".ultimo").detach();
    $("#ul2").append(elemento);
}

function aggiungiDescrizione() {
    $("div.descrizione").html("Testo da aggiungere alla descrizione");
}

function nascondiUL() {
    $("ul").hide(1000);
}

function visualizzaUL() {
    $("ul").show(1000);
}




$(document).ready(function() {
    var pulsante = $("a#pulsante2");
    pulsante.click(function(){
        cambiaColorePrimoLi("green");
        return false;
    });
    $("a#pulsante3").bind("click", "pink", cambiaColorePrimoLi);

    // aggiungo pulsante classe lc
    $("div#contenitore").append("<a id='pulsante4' href='#'>Aggiungi classe lc</a>");
    $("a#pulsante4").click(toggleClassLC);

    $("div#contenitore").append("<a id='pulsante5' href='#'>Sposta elemento</a>");
    $("a#pulsante5").click(spostaElemento);

    $("div#contenitore").append("<a id='pulsante6' href='#'>Aggiungi descrizione</a>");
    $("a#pulsante6").click(aggiungiDescrizione);

    $("div#contenitore").append("<a id='pulsante7' href='#'>Nascondi ul</a>");
    $("a#pulsante7").click(nascondiUL);

    $("div#contenitore").append("<a id='pulsante8' href='#'>Visualizza ul</a>");
    $("a#pulsante8").click(visualizzaUL);

});