var colori;
colori = ["rosso", "giallo", "verde"];

console.log("colori[0] = " + colori[0]);
console.log("colori[1] = " + colori[1]);
console.log("colori[2] = " + colori[2]);
console.log("colori[2] = " + colori[3]); // undefined perchè non è specificato un quarto valore
console.log(colori); //trasforma l'array in una stringa
console.log("colori: "+colori)

colori[3]="blu"; // inserisco una stringa all'indice [3] dell'array
colori.push("marrone"); //inserisce un valore nell'ultima posizione dell'array libera
colori.push(150);
console.log(colori);

console.log("--------------");

console.log(typeof colori[0]); //typeof restituisce una stringa con il tipo della variabile
console.log(typeof colori[5]);
console.log(typeof colori);

console.log("----NESTED ARRAY----");

colori.push([1,2,3,4,5]);
console.log(colori);
console.log("colori[6][2] = " + colori[6][2]);