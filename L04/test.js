// commento linea singola
/* commento 
multilinea */

//per eseguire il codice nel terminale "node nomefile.js"

/* stampo nome e cognome */
var nome; //dichiarazione della variabile
nome = "Lorenzo"; //inizializzazione della variabile

var cognome = "Pretto"; //dichiarazione e inizializzazione su un'unica riga

var eta = 22;

console.log("Esercizio"); //stampa la stringa
console.log(nome); //stampa il contenuto della variabile
console.log(nome + " " + cognome + " " + eta);
console.log("tra dieci anni: "+(eta + 10));
console.log("eta / 2 = " + (eta/2));
console.log("eta * 2 = " + (eta * 2));
console.log("eta % 2 = " + (eta % 2)); //operatore modulus, restituisce il resto della divisione; utile per controllare se un numero è pari o dispari
//Gli operatori ++ e -- possono essere posizionati sia prima che dopo la variabile
//La differenza è il quando viene eseguita l'operazione, se prima o dopo che la variabile venga letta
console.log("eta -= 10 = " + (eta-=10));
console.log("eta += 10 = " + (eta+=10));