var citta = "Milano";
console.log("il valore della variabile citta=\""+citta+"\" è di tipo " + typeof citta);

var numero = 8+8/3;
console.log("il valore della variabile numero="+numero+" è di tipo " + typeof numero);

var lista = ["uno", 2, 3+1]
console.log("il valore della variabile lista="+lista+" è di tipo " + typeof lista);

var vuota;
console.log("il valore della variabile vuota="+vuota+" è di tipo " + typeof vuota);

var nulla = null;
console.log("Il valore della variabile nulla è di tipo " + typeof nulla);

var operazione = "Uno" + 2;
console.log("\"Uno\"+2 è di tipo " + typeof operazione);

var nonNumero = NaN;
console.log("Il valore NaN è di tipo " + typeof nonNumero);

console.log("il valore di \"false\" è "  + typeof "false");
console.log("il valore true è di tipo " + typeof true);